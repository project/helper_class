<?php

namespace Drupal\helper_class\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Unformatted style with just wrapper and helper class.
 *
 * Row are rendered with wrappers and helper class.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "helper_class_default",
 *   title = @Translation("Helper class"),
 *   help = @Translation("Handle rows and wrapper with helper class."),
 *   theme = "views_view_helper_class_style",
 *   display_types = {"normal"}
 * )
 */
class HelperClass extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;


  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['row_helper_class'] = ['default' => ''];
    $options['rows_wrapper_helper_class'] = ['default' => ''];
    $options['title_helper_class'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['title_helper_class'] = [
      '#title' => $this->t('Title CSS classes'),
      '#description' => $this->t('Title helper classes.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['title_helper_class'],
    ];

    $form['rows_wrapper_helper_class'] = [
      '#title' => $this->t('Row wrapper CSS classes'),
      '#description' => $this->t('Class of the direct parent wrapper of each row.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['rows_wrapper_helper_class'],
    ];

    $form['row_helper_class'] = [
      '#title' => $this->t('Row CSS classes'),
      '#description' => $this->t('The class to provide on each row.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['row_helper_class'],
    ];

  }

}
