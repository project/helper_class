<?php

namespace Drupal\Tests\helper_class\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\BrowserTestBase;

/**
 * Define entity type for test.
 */
const ENTITY_TYPE_TEST = 'article';

/**
 * Define prefix for helper class tests.
 */
const CSS_CLASS_TEST_PREFIX = 'helper-class--';


/**
 * Tests the helper class settings on entity view displays.
 *
 * @group helper_class
 */
class EntityHelperClassTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'helper_class',
    'field',
    'node',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => ENTITY_TYPE_TEST, 'name' => 'Article']);

    // Full node article display.
    $display = EntityViewDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => ENTITY_TYPE_TEST,
      'mode' => 'full',
      'status' => TRUE,
    ]);

    $display->save();

  }

  /**
   * Set display settings for entity.
   */
  public function setupEntityDisplay() {
    $display = EntityViewDisplay::load('node.' . ENTITY_TYPE_TEST . '.full');
    $display->setThirdPartySetting('helper_class', 'helper_class_values', CSS_CLASS_TEST_PREFIX . ENTITY_TYPE_TEST);
    $display->save();

  }

  /**
   * Test helper class.
   */
  public function testHelperClass() {
    $this->setupEntityDisplay();

    $node = $this->drupalCreateNode([
      'type' => ENTITY_TYPE_TEST,
      'title' => 'Test node',
    ]);

    $this->drupalGet($node->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', '.' . CSS_CLASS_TEST_PREFIX . ENTITY_TYPE_TEST);
  }

}
